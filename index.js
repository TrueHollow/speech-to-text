'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const config = require('config');
const log4js = require('log4js');
const logger = require('./common/Logger');

const app = express();
app.use(log4js.connectLogger(logger, {
    level: 'auto',
    format: (req, res, format) => format(`:remote-addr ":method :url HTTP/:http-version" :status :content-length ":referrer" ":user-agent"`)
}));

logger.info('Script running.');

const http_port = config.get('http_server.http_port');
app.listen(http_port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(fileUpload());

app.get('/api/v1/audio/list/', require('./api/v1/audio/list'));
app.get('/api/v1/audio/:name', require('./api/v1/audio/get'));
app.post('/api/v1/audio/post/', require('./api/v1/audio/post'));

logger.info(`app running on port http://localhost:${http_port}...`);
module.exports = app;
