## Preparation

### Node version

Tested on

```sh
node --version
v11.10.1
```


### Create configuration file for your enviroment

Create custom configuration file in `config` folder with

```json
{
  "google_cloud": {
    "speech": {
      "api_key": "cloud_api_key"
    },
    "storage": {
      "projectId": "project-id",
      "keyFilename": "/path/to/keyfile.json"
    }
  },
  "log4js": {
    "appenders": {
      "out": {
        "type": "stdout"
      }
    },
    "categories": {
      "default": {
        "appenders": [
          "out"
        ],
        "level": "debug"
      }
    }
  },
  "http_server": {
    "http_port": 3000
  }
}
```

and update environment variable `NODE_ENV`

### Install dependencies

Run command ```npm install```

### Running tests

Execute command ```npm test```

## Running app

Execute command ```npm start```