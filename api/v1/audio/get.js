'use strict';

const config = require('config');
const logger = require('../../../common/Logger');
const request = require('request');

const YOUR_API_KEY = config.get("google_cloud.speech.api_key");

const getAudioResult = (text_id) => {
    const url = `https://speech.googleapis.com/v1/operations/${text_id}?key=${YOUR_API_KEY}`;
    return new Promise((resolve, reject) => {
        request({
            url: url,
            method: 'GET',
            json: true
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = async function(req, res) {
    try {
        const result = await getAudioResult(req.params.name);
        return res.json(result.body);
    } catch (e) {
        logger.error(e);
        return res.status(500).json({err: "internal error"});
    }
};