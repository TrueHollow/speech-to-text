'use strict';

const config = require('config');
const logger = require('../../../common/Logger');
const request = require('request');

const getRequestObject = (url) => {
    return {
        "config": {
            "languageCode": "en-US"
        },
        "audio": {
            "uri": url
        }
    };
};

const YOUR_API_KEY = config.get("google_cloud.speech.api_key");
const url = `https://speech.googleapis.com/v1/speech:longrunningrecognize?key=${YOUR_API_KEY}`;

const sendAudio = async (data) => {
    return new Promise((resolve, reject) => {
        request({
            url: url,
            method: 'POST',
            body: data,
            json: true
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

const {Storage} = require('@google-cloud/storage');
const fs = require('fs');

const uploadsDir = './uploads';

const createUploadsDirectory = async () => {
    return new Promise((resolve, reject) => {
        fs.mkdir(uploadsDir, { recursive: true }, (err) => {
            if (err) {
                reject(uploadsDir);
            } else {
                resolve();
            }
        });
    });
};

const uniqueFilename = require('unique-filename');

const saveTempFile = async (buffer) => {
    return new Promise((resolve, reject) => {
        const randomTmpfile = `${uniqueFilename(uploadsDir)}.flac`;
        fs.writeFile(randomTmpfile, buffer, err => {
            if (err) {
                reject(err);
            } else {
                resolve(randomTmpfile);
            }
        })
    });
};

const removeTempFile = async (path) => {
    return new Promise((resolve, reject) => {
        fs.unlink(path, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    })
};

module.exports = async function(req, res) {
    try {
        const files = req.files;
        if (!files || !files.audioFile) return res.json({err: "no file"});

        const credentials = config.get("google_cloud.storage");
        const storage = new Storage(credentials);
        const bucketName = 'bucketaudiotest';

        const file = files.audioFile;
        const buffer = file.data;
        await createUploadsDirectory();
        const fileLocal = await saveTempFile(buffer);

        const bucket = await storage.bucket(bucketName);
        const [fileCloud, apiResponseFile] = await bucket.upload(fileLocal);

        await fileCloud.makePublic();
        await removeTempFile(fileLocal);

        const gsUrl = `gs://${bucketName}/${fileCloud.id}`;
        const data = getRequestObject(gsUrl);
        const result = await sendAudio(data);
        await fileCloud.delete();
        return res.json(result);
    } catch (e) {
        logger.error(e);
        return res.status(500).send({err: "internal error"});
    }
};