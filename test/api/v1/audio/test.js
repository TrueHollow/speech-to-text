'use strict';

const fs = require('fs');
const path = require('path');
const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../../../index');
const logger = require('../../../../common/Logger');

describe('Audio tests', () => {
    it('test post audio file', async () => {
        const urlPath = '/api/v1/audio/post';
        const pathToFile = path.join(__dirname, 'speech.flac');
        const res = await chai.request(app).post(urlPath).attach('audioFile', fs.readFileSync(pathToFile), 'speech.flac');
        expect(res).to.have.status(200);
        const result = res.body;
        logger.info(JSON.stringify(result));
        expect(result).to.be.an('object');
    });
    it('test list', async () => {
        const urlPath = '/api/v1/audio/list';
        const res = await chai.request(app).get(urlPath);
        expect(res).to.have.status(200);
        const result = res.body;
        logger.info(JSON.stringify(result));
        expect(result).to.be.an('object');
    });
    it('test get', async () => {
        const urlPath = '/api/v1/audio/some_id';
        const res = await chai.request(app).get(urlPath);
        expect(res).to.have.status(200);
        const result = res.body;
        logger.info(JSON.stringify(result));
        expect(result).to.be.an('object');
    });
});